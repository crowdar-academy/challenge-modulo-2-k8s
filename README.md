# Web Bookinfo 

Para este challenge final deberán levantar una aplicación básica de libros con la siguiente arquitectura:
 
![Challenge](challenge-k8s.png)
 
## Lineamientos
 
Crea un repo por cada componente dentro de un grupo de gitlab.
Cada repo tiene que tener su pipeline con build, hadolint y trivy. Tiene que subir la imagen a la registry de gitlab, para poder desplegarce desde la registry de gitlab.
 
Por último se creara un repo con los yaml para desplegar en kubernetes
 
## Componentes
 
Ingress -> ProductPage -> Deployment ( 2 Replicas ) [ Port: 9080 ] [ Health: /health ]
 
Ratings -> Deployment ( 2 Replicas ) [ Port: 9080 ] [ Health: /health ]
 
Reviews -> Deployment ( 2 Replicas ) [ Port: 9080 ] [ Health: /health ]
 
Details -> Deployment ( 2 Replicas ) [ Port: 9080 ] [ Health: /health ]
 
MongoDB -> StateFullSet ( 1 Replica ) [ Port: 27017 ]
 
Todos los Variables de entornos de configuración tiene que ser configmaps y las contraseñas o datos sensibles en secrets.
 
Cada componente tiene que utilizar su propio namespaces, con sus correspondientes servicios, secrets y configmaps

Adicional a eso, se deberá agregar el secreto que tenga las credenciales para autenticar con la registry privada del repositorio donde se encuentran trabajando.


## Build componentes

- Details -> Dockerfile
``` bash
docker build -t "${REGISTRY_NAME}:${VERSION}" --build-arg service_version=v2 --build-arg enable_external_book_service=true .
```

- ProductPage -> Dockerfile
``` bash
docker build -t "${REGISTRY_NAME}:${VERSION}" --build-arg flood_factor=100 .
```

- Ratings -> Dockerfile
``` bash
docker build -t "${REGISTRY_NAME}:${VERSION}" --build-arg service_version=v2 .
```

- Reviews -> Dockerfile
``` bash
cd reviews-wlpcfg
docker build -t "${REGISTRY_NAME}:${VERSION}" --build-arg enable_ratings=true --build-arg star_color=red .
```

- Mongo -> Dockerfile
``` bash
docker build -t "${REGISTRY_NAME}:${VERSION}" .
```


## Env de los containers

### ProductPage

* SERVICES_DOMAIN = ( Sufijo general del servicio)
* DETAILS_HOSTNAME = ( Servicio de DETAILS )
* RATINGS_HOSTNAME = ( Servicio de RATINGS )
* REVIEWS_HOSTNAME = ( Servicio de REVIEWS )

### Ratings

* SERVICE_VERSION = 'v2'
* DB_TYPE = 'mongodb'
* MONGO_DB_URL = ( mongo://mongodbhost:20017 )

### Reviews

* ENABLE_RATINGS = 'true'
* STAR_COLOR = 'red'
* SERVICES_DOMAIN = ( Sufijo general del servicio)
* RATINGS_HOSTNAME = ( Servicio de RATINGS )

### Details

ENABLE_EXTERNAL_BOOK_SERVICE = 'true'
